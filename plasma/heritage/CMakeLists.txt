install(FILES colors metadata.desktop DESTINATION ${CMAKE_INSTALL_PREFIX}/share/plasma/desktoptheme/heritage/)
install(DIRECTORY dialogs icons translucent wallpapers weather widgets DESTINATION ${CMAKE_INSTALL_PREFIX}/share/plasma/desktoptheme/heritage)
